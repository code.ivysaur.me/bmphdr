# bmphdr

![](https://img.shields.io/badge/written%20in-C-blue)

A command-line utility to write out custom BMP headers.

Designed as a companion to a framebuffer dumping system.

## Changelog

2011-08-14
- Initial private release


## Download

- [⬇️ bmphdr-win32.7z](dist-archive/bmphdr-win32.7z) *(4.02 KiB)*
- [⬇️ bmphdr-src.7z](dist-archive/bmphdr-src.7z) *(1.41 KiB)*
